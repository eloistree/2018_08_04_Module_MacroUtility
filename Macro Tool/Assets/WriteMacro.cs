﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput;

public class WriteMacro : MonoBehaviour {


	// Use this for initialization
	void Start () {
        InputSimulator input = new InputSimulator();
        input.Keyboard.KeyDown(WindowsInput.Native.VirtualKeyCode.LMENU);
        input.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.TAB);
        input.Keyboard.KeyUp(WindowsInput.Native.VirtualKeyCode.LMENU);
        input.Mouse.MoveMouseTo(Screen.currentResolution.width, Screen.currentResolution.height);
        input.Mouse.RightButtonClick(); 

    }


    // Update is called once per frame
    void Update () {
		
	}
}
